ARG OPENJDK_TAG=8u212
FROM openjdk:${OPENJDK_TAG}

ARG SBT_VERSION=1.2.8

# Install sbt
RUN \
  curl -L -o sbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  apt-get update && \
  apt-get install sbt && \
  sbt sbtVersion

#install git
RUN apt-get install git

# install g++ needed by some npm packages
RUN apt-get install -y g++ build-essential

# install node
RUN apt-get update && \
  apt-get -y install curl gnupg && \
  curl -sL https://deb.nodesource.com/setup_11.x  | bash -
RUN apt-get -y install nodejs && \
  node -v && \
  npm -v